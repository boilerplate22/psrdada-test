# include base makefile
include .make/base.mk


# include oci support
include .make/oci.mk

# include repo specific override residing in current repository
-include Override.mk

# include workstation specific targets
-include WorkstationTargets.mk
